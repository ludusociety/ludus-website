::Spina::Theme.register do |theme|

  theme.name = 'default'
  theme.title = 'Default Theme'

  theme.page_parts = [
    {
      name:           'slogan',
      title:          'Slogan',
      partable_type:  'Spina::Line'
    },
    {
      name:           'block_1',
      title:          'Block_1',
      partable_type:  'Spina::Text'
    },
    {
      name:           'text',
      title:          'Text',
      partable_type:  'Spina::Text'
    },
    {
      name:           'summary',
      title:          'Summary',
      partable_type:  'Spina::Text'
    }
  ]

  theme.view_templates = [
    {
      name:       'homepage',
      title:      'Homepage',
      page_parts: ['slogan', 'text']
    },
    {
      name:         'article',
      title:        'Article',
      description:  'Pages with content',
      usage:        'Use for building content in format of posts/articles/news',
      page_parts:   ['summary', 'text']
    },
    {
      name:       'about',
      title:      'About',
      page_parts: ['text']
    },
  ]

  theme.custom_pages = [
    {
      name:           'homepage',
      title:          'Homepage',
      deletable:      false,
      view_template:  'homepage'
    }
  ]

  theme.navigations = [
    {
      name: 'mobile',
      label: 'Mobile'
    },
    {
      name: 'main',
      label: 'Main navigation',
      auto_add_pages: false
    }
  ]

end
