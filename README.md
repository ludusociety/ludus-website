# Ludus Website

The LudusWebsite is a Rails application that works as a CMS. It works
based on [Spina](http://www.spinacms.com/) framework.

We are using the [Materializer CSS framework](https://materializecss.com/getting-started.html)

All the content is stored in database. You will need an admin user to insert
or change content.

## Setup

* Install ruby 2.7.1 - You may use RVM for that
* Install native dependencies:
```sh
sudo apt install imagemagick libpq-dev
```
* Install [volta](https://docs.volta.sh/guide/getting-started) to manage
node and yard versions
* Use volta to install the latest node and yarn:
```sh
volta install node
volta install yarn
```
* Install docker and docker-compose
* Run the postgresql with docker-compose (you may pass '-d' to run as deamon:
```sh
docker-compose up
```
* Create database:
```sh
rake db:create
```
* Run the spina generator to run migrations and create an admin user:
```sh
rails g spina:install
```
* Run the application
```sh
rails s
```

You should be able to access the following pages:
* homepage: http://localhost:3000/
* admin-page: http://localhost:3000/admin/


## References:

* [MDL Components](https://getmdl.io/components/#lists-section)
* [MDL Colors](http://blog.jonathanargentiero.com/material-design-lite-color-classes-list/)
* [Template MDL 1](https://getmdl.io/templates/text-only/index.html)
* [Template MDL 2](https://getmdl.io/templates/portfolio/index.html)
* [Template MDL 3](https://templateflip.com/demo/templates/material-portfolio/)
* [Template MDL 4](https://demo.templateflip.com/material-portfolio/)
